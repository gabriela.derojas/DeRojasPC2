﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boton : MonoBehaviour
{
    public Text anuncio_text;
    private bool state = true;
    public Character pj;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void anuncio()
    {
        if(state)
        {
            this.anuncio_text.gameObject.SetActive(true);
            this.state = false;
            this.pj.state_control = true;
        }
        else if(!state)
        {
            this.anuncio_text.gameObject.SetActive(false);
            this.state = true;
            this.pj.state_control = false;
        }
        
    }
}
