﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    private float side;
    // Start is called before the first frame update
    void Start()
    {
        this.side = this.transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.side < 3)
        {
            this.transform.Translate(speed * Time.deltaTime, 0, 0);
            if(this.transform.position.x >= 3)
            {
                Destroy(this.gameObject);
            }
        }else if (this.side > -3)
        {
            this.transform.Translate(-speed * Time.deltaTime, 0, 0);
            if (this.transform.position.x <= -3)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
