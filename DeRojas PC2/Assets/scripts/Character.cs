﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public int life;
    public Text Life;
    public int puntos;
    public Text Score;
    private Rigidbody2D rig_2d;
    Touch touch;
    private Vector2 first_press;
    public bool state_control = false;
    public float speed;
    private int direction;
    // Start is called before the first frame update
    void Awake()
    {
        rig_2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!state_control)
        {
            if (Input.touchCount == 1)
            {
                touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    first_press = touch.position;
                }
                else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
                {
                    Vector2 direction = touch.position - first_press;
                    if (direction.y > 0.5f && direction.y >= Mathf.Abs(direction.x) * 2)
                    {
                        //up
                        this.transform.position = Vector2.MoveTowards(this.transform.position, direction, 3.0f * Time.deltaTime);
                    }
                    else if (direction.y < -0.5f && direction.y <= -Mathf.Abs(direction.x) * 2)
                    {
                        //down
                        this.transform.position = Vector2.MoveTowards(this.transform.position, direction, 3.0f * Time.deltaTime);
                    }
                }

            }
        }
        if(state_control)
        {
            if(Input.acceleration.y < -0.05f)
            {
                direction = -1;
            } else if (Input.acceleration.y > 0.05f)
            {
                direction = 1;
            }else if (Input.acceleration.y < -0.05f && Input.acceleration.y > 0.05f)
            {
                direction = 0;
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
            this.life--;
            this.Life.text = this.life.ToString();
        }
        if (collision.gameObject.tag == "fruta")
        {
            Destroy(collision.gameObject);
            this.puntos = this.puntos + 10;
            this.Score.text = this.puntos.ToString();
        }
    }
}
