﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemy;
    public GameObject fruta;
    public float minY, maxY, left_X, right_X, SpawRate_min, SpawnRate_max;
    private float spawnRate;
    private int rnd, frutaRate;
    // Start is called before the first frame update
    void Start()
    {
        SpawnEnemies();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void SpawnEnemies()
    {
        this.frutaRate = Random.Range(0, 10);
        rnd = Random.Range(0, 2);
        if(this.rnd == 0)
        {
            if(this.frutaRate > 7)
            {
                Vector2 spawn_point = new Vector2(this.left_X, Random.Range(this.minY, this.maxY));
                Instantiate(fruta, spawn_point, transform.rotation);
            }
            else
            {
                Vector2 spawn_point = new Vector2(this.left_X, Random.Range(this.minY, this.maxY));
                Instantiate(enemy, spawn_point, transform.rotation);
            }
        }
        else if(this.rnd == 1)
        {
            if (this.frutaRate > 7)
            {
                Vector2 spawn_point = new Vector2(this.right_X, Random.Range(this.minY, this.maxY));
                Instantiate(fruta, spawn_point, transform.rotation);
            }
            else
            {
                Vector2 spawn_point = new Vector2(this.right_X, Random.Range(this.minY, this.maxY));
                Instantiate(enemy, spawn_point, transform.rotation);
            }
        }
        this.spawnRate = Random.Range(this.SpawRate_min, this.SpawnRate_max);
        Invoke("SpawnEnemies", this.spawnRate);
    }
}
